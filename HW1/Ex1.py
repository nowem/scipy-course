import random
import numpy as np
import matplotlib.pyplot as plt


def step(SIR, α=0.1, β=0.2):
    S, I, R = SIR

    infected = np.sum(np.random.random(size=S) < α)
    recovered = np.sum(np.random.random(size=I) < β)

    S -= infected
    I += infected - recovered
    R += recovered

    return np.array([S, I, R])


def simulation(SIR0, α=0.1, β=0.2, days=30):
    SIR = np.empty((days+1, 3), dtype=int)
    SIR[0] = np.array(SIR0)

    for i in range(1, days + 1):
        SIR[i] = np.array(step(SIR[i-1], α, β), dtype=int)

    return SIR


days = 90
SIR = simulation([100, 0, 0], α=0.1, β=0.2, days=days)
num_days = np.arange(0, days + 1)

S = SIR[:, 0]
I = SIR[:, 1]
R = SIR[:, 2]

plt.xlabel('Days')
plt.ylabel('# of individuals')

plt.plot(num_days, S, color='red', label='S')
plt.plot(num_days, I, color='blue', label='I')
plt.plot(num_days, R, color='green', label='R')
plt.legend()

plt.show()
