import random
import numpy as np
import matplotlib.pyplot as plt


def step(SIR, α=0.1, β=0.2):
    S, I, R = SIR

    infected = (np.random.random(size=S) < α).sum()
    recovered = (np.random.random(size=I) < β).sum()

    S -= infected
    I += infected - recovered
    R += recovered

    return np.array([S, I, R])


def simulation(SIR0, α=0.1, β=0.2, days=30, reps=100):
    assert reps > 0

    SIR = np.zeros((reps, days+1, 3), dtype=int)
    SIR[:, 0, :] = np.array([SIR0 for _ in range(reps)])

    for i in range(1, days + 1):
        SIR[:, i, :] = np.array([step(SIRK, α, β)
                                 for SIRK in SIR[:, i - 1, :]])

    return SIR


reps = 100
days = 91
SIR0 = [100, 0, 0]

N = sum(SIR0)
num_days = np.arange(days + 1)

SIR = simulation(SIR0, days=days, reps=reps)

S = SIR[:, :, 0]
I = SIR[:, :, 1]
R = SIR[:, :, 2]

ratio_I = ((I + R) / N) >= .5
ratio_R = (R / N) >= .5

T_I = int(sum(map(np.argmax, ratio_I)) / reps)
T_R = int(sum(map(np.argmax, ratio_R)) / reps)


print("T_I = ", T_I)
print("T_R = ", T_R)

plt.axvline(T_I, linestyle='--', color='black')
plt.axvline(T_R, linestyle='--', color='black')

plt.axhline(SIR0[0] / 2, color='black')
plt.axhline(SIR0[1] / 2, color='black')
plt.axhline(SIR0[2] / 2, color='black')


# Create plots for ach replication
for rep in range(reps):
    S = SIR[rep, :, 0]
    I = SIR[rep, :, 1]
    R = SIR[rep, :, 2]

    plt.plot(num_days, S, color='red', alpha=0.1)
    plt.plot(num_days, I, color='blue', alpha=0.1)
    plt.plot(num_days, R, color='green', alpha=0.1)

# Initialize average dynamics
avg = SIR.sum(0) / reps

plt.plot(num_days, avg[:, 0], color='black', alpha=0.8)
plt.plot(num_days, avg[:, 1], color='black', alpha=0.8)
plt.plot(num_days, avg[:, 2], color='black', alpha=0.8)

plt.xlim((0, 30))

plt.xlabel('Days')
plt.ylabel('# of individauls')
plt.show()
