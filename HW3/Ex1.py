import matplotlib.pyplot as plt
import matplotlib as mpl
plt.warnings.simplefilter('ignore', FutureWarning)
import numpy as np
import statsmodels.api as sm
import urllib.request
import zipfile
import os.path
import pandas as pd
import seaborn as sns
sns.set_context('notebook')
sns.set_palette('muted')

red, blue, green = sns.color_palette('Set1', 3)


x = np.array([46, 51, 54, 57, 59, 61, 63, 66, 68, 72])
y = np.array([40, 55, 72, 77, 90, 96, 99, 113, 127, 132])

plt.plot(x, y, 'o')
plt.xlabel('Fahrenheit')
plt.ylabel('Chirps / min')
sns.despine()

X = sm.add_constant(x)
link = sm.families.links.log()
family = sm.families.Poisson(link=link)
result_poi = sm.GLM(y, X, family=family).fit()

b, a = result_poi.params
print('a={:.4f}, b={:.2f}'.format(a, b))

yhat = np.exp(x * a + b)
plt.plot(x, yhat)
plt.show()
