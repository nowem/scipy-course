import matplotlib.pyplot as plt
import matplotlib as mpl
plt.warnings.simplefilter('ignore', FutureWarning)
import numpy as np
import statsmodels.api as sm
import urllib.request
import zipfile
import os.path
import pandas as pd
import seaborn as sns
sns.set_context('notebook')
sns.set_palette('muted')

red, blue, green = sns.color_palette('Set1', 3)


x = np.array([46, 51, 54, 57, 59, 61, 63, 66, 68, 72])
y = np.array([40, 55, 72, 77, 90, 96, 99, 113, 127, 132])

X = sm.add_constant(x)
link = sm.families.links.log()
family = sm.families.Poisson(link=link)
result_poi = sm.GLM(y, X, family=family).fit()
b, a = result_poi.params
print(a, b)


import scipy.special


def loglik(a, b):
    yhat = np.exp(a * x + b)
    return (np.log(yhat) - yhat).sum(1)


arange = np.broadcast_to(np.linspace(-1, 1, 200), (200, 200, 10))
brange = np.broadcast_to(np.linspace(-4, 4, 200), (200, 200, 10))
# ll = np.array([[loglik(a, b) for b in brange] for a in arange])
ll = np.array(loglik(arange, brange))


def gradient(a, b):
    yhat = np.exp(a * x + b)
    da = (x*y - yhat * x).sum()
    db = (y - yhat).sum()
    η = 0.0001
    return a - η * da, b - η * db


def gradient_check(a, b, ϵ=1e-4):
    da, db = gradient(a, b)
    # print(f'a={a},  b={b}')
    # print(f'da={da}, db={db}')

    ll_plus, ll_minus = loglik(a+ϵ, b), loglik(a-ϵ, b)
    da_ = (ll_plus - ll_minus) / (2 * ϵ)
    ll_plus, ll_minus = loglik(a, b+ϵ), loglik(a, b-ϵ)
    db_ = (ll_plus - ll_minus) / (2 * ϵ)
    rel_err_a = abs(da - da_)/da
    rel_err_b = abs(db - db_)/db
    if rel_err_a > 2*ϵ or rel_err_b > 2*ϵ:
        msg = 'a={:e}, b={:e}\na relerr={:e}, b relerr={:e}'
        print(msg.format(a, b, rel_err_a, rel_err_b))


a_, b_ = 0, 0
for _ in range(1000):
    a_, b_ = np.random.normal(a_, 0.01), np.random.normal(b_, 0.1)
    gradient_check(a_, b_)

# don't change this cell
plt.pcolormesh(brange.squeeze(), arange.squeeze(),
               ll, norm=mpl.colors.SymLogNorm(1e-5))
plt.plot(b, a, 'ok')
plt.xlabel('intercept, $b$')
plt.ylabel('slope, $a$')
plt.colorbar(label='log-likelihood', ticks=-np.logspace(2, 31, 6))
plt.show()
