import matplotlib.pyplot as plt
plt.warnings.simplefilter('ignore', FutureWarning)
import numpy as np
import scipy
import scipy.constants
import pandas as pd
import seaborn as sns
import urllib.request
import zipfile
import os.path


url = 'http://genomics.senescence.info/species/dataset.zip'
fname = '../data/anage_dataset.zip'
if not os.path.exists(fname):
    urllib.request.urlretrieve(url, fname)
print("Data file exists:", os.path.exists(fname))

with zipfile.ZipFile(fname) as z:
    f = z.open('anage_data.txt')
    data = pd.read_table(f)


temp_c = 'Temperature (C)'
data['Temperature (C)'] = scipy.constants.convert_temperature(
    data['Temperature (K)'], 'Kelvin', 'Celsius')

data = data[np.isfinite(data[temp_c])]

mammals_temp = data[data['Class'] == 'Mammalia'][temp_c]
amphibia_temp = data[data['Class'] == 'Amphibia'][temp_c]

tstat, p_value = scipy.stats.ttest_ind(
    mammals_temp, amphibia_temp, equal_var=False)
H0_rejected = p_value < 0.05

print("The temperatures of mammals and amphibians are different? {}".format(H0_rejected))
print("P-value: {:.2g}".format(p_value))

animal_classes = data['Class'].unique()
filtered_classes = list(filter(
    lambda x: data[data['Class'] == x]['Species'].unique().shape[0] >= 10,
    animal_classes
))

fdata = data[data['Class'].isin(filtered_classes)]

bins = np.linspace(0, 42, 42)
figure = sns.FacetGrid(fdata, col='Class', margin_titles=True, sharey=False)
figure.map(plt.hist, temp_c, color='steelblue', bins=bins, alpha=0.5)
figure.set(ylim=(0, None))

# # data['Temperature (C)'].hist(bins=np.arange(
#     data[temp_c].min(), data[temp_c].max()))
plt.xlabel('Temperature (C)')
plt.ylabel('# of Species')
plt.show()
