import matplotlib.pyplot as plt
plt.warnings.simplefilter('ignore', FutureWarning)
import numpy as np
import scipy
import scipy.constants
import pandas as pd
import seaborn as sns
import urllib.request
import zipfile
import os.path


fname = '../data/txtdata.csv'
data = np.loadtxt(fname).astype(int)
days = len(data)


from scipy.stats import poisson


# def hist_poisson(data):
#     λ = data.mean()
#     poi = poisson(λ)
#     hist, bins, _ = plt.hist(data, bins=20, density=True, alpha=0.7)
#     xrange = range(0, data.max())
#     plt.plot(xrange, poi.pmf(xrange), '-k')
#     plt.xlabel('# of texts recieved')
#     plt.ylabel('frequency')
#     sns.despine()
#     plt.show()
#     print("Average number of texts received {:.2f}".format(λ))


# hist_poisson(data)
# hist_poisson(np.random.poisson(data.mean(), size=days))


def loglik(t):
    l1 = data[:t].mean() if len(data[:t]) > 0 else 0
    l2 = data[t:].mean() if len(data[t:]) > 0 else 0

    p1 = poisson(l1)
    p2 = poisson(l2)

    preswitch = np.sum([p1.logpmf(k) for k in range(t)])
    postswitch = np.sum([p2.logpmf(k) for k in range(t, days)])

    loglik_t = preswitch + postswitch

    return loglik_t


loglik_t = np.array([loglik(t) for t in range(days)])
tau = np.argmax(loglik_t)

print('Most likely switch day:', tau)
print('Number of days: ', days)

plt.plot(loglik_t, '-ok')
plt.plot(tau, loglik_t[tau], '-ok', color='red')
plt.show()


# plt.bar(range(days), data, align='edge')
# plt.xlabel('Day')
# plt.ylabel('# of texts received')
# plt.xlim(0, days)
# sns.despine()
# plt.show()
