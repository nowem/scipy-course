import matplotlib.pyplot as plt
plt.warnings.simplefilter('ignore', FutureWarning)
import numpy as np
import scipy
import pandas as pd
import seaborn as sns
import urllib.request
import zipfile
import os.path


url = 'http://genomics.senescence.info/species/dataset.zip'
fname = '../data/anage_dataset.zip'
if not os.path.exists(fname):
    urllib.request.urlretrieve(url, fname)
print("Data file exists:", os.path.exists(fname))

with zipfile.ZipFile(fname) as z:
    f = z.open('anage_data.txt')
    data = pd.read_table(f)

# handy aliases
litter = 'Litter/Clutch size'
weight = 'Birth weight (g)'

# Remove NaN rows
data = data[np.isfinite(data['Litter/Clutch size'])]
data = data[np.isfinite(data['Birth weight (g)'])]

assert not np.isnan(data['Litter/Clutch size']).any()
assert not np.isnan(data['Birth weight (g)']).any()

data[weight] = np.log(data[weight])
data[litter] = np.log(data[litter])

slope, intercept, r_value, p_value, _ = scipy.stats.linregress(
    x=data[litter], y=data[weight])

print("intercept: {:.3f}, slope: {:.3f}".format(intercept, slope))

signif = ('' if abs(r_value) >= 0.5 else 'not ') + 'significant'
direction = 'positive' if r_value > 0 else 'negative'

print('r_value = ', r_value)
print('p_value = ', p_value)
print(f'Litter size has {signif} and {direction} effect on birth weight')


num_offspings = 10
prediction = np.exp(intercept + slope * np.log(num_offspings))
print('In a litter with 10 offspring, the birth weight will be {} grams'.format(prediction))


data.plot(x='Litter/Clutch size', y='Birth weight (g)', style='o', alpha=0.4)
sns.lmplot(x=litter, y=weight, data=data)

plt.xlabel('Litter/Clutch size')
plt.ylabel('Birth weight (g)')
plt.show()
